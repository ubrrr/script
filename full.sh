#!/bin/sh

if [ "$(id -u)" = 1000 ]; then
   echo Must be root to run this script
   exit 1
fi
error() { \
   clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}

while true ;do
read -p "
Choose keymap:
1) us
2) fr
Select [us] or [fr]: " INFO
case $INFO in
   "us" ) break;;
   "fr" ) break;;
   "1" ) break;;
   "2" ) break;;
   * ) echo "
Invalid option: $INFO";;
   esac
done

echo "
### HOSTNAME CONFIGURATION ###"
read -p "Create Hostname: " HOST
echo "
### ROOT PASSWORD ###"
while true ;do
read -sp "Set root password: " PASS
read -sp "
Repeat root password: " PASS1

# Check if passwords match!
if [ "$PASS" = "$PASS1" ]; then
   break
else
   echo "

Passwords do not match!
"
fi
done

echo "

### USER CONFIGURATION ###"
read -p "Create username: " UN

while true ;do
read -sp "
Set $UN password: " UPASS
read -sp "
Repeat $UN password: " UPASS1

# Check if user passwords match!
if [ "$UPASS" = "$UPASS1" ]; then
   break
else
   echo "

Passwords do not match!"
fi
done

while true ;do
echo
echo
lsblk
read -p "
Select a drive to format, example [sda]: " DRIVE
case $DRIVE in
    sd? ) [ -b /dev/$DRIVE ] && break;;
    vd? ) [ -b /dev/$DRIVE ] && break;;
    nvme??? ) [ -b /dev/$DRIVE ] && break; export SUB=p;;
esac

if [ -b /dev/$DRIVE ]; then
echo "
[$DRIVE] is a partition. Please select a drive!"
else
echo "
Drive [$DRIVE] does not exist!"
fi

done

while true; do
echo -e "
\e[31mWARNING!!!\e[0m Are you sure you want to format drive [$DRIVE]?
\e[31mWARNING!!!\e[0m All the data in the drive will be destroyed and
cannot be recovered!"
read -p "Are you sure you want to proceed? [yes/no]: " yn
    case $yn in
        [Yy][Ee][Ss] ) wipefs -fa /dev/$DRIVE; break;;
        [Nn][Oo] ) echo "
A drive must be formated in order to install Arch Linux!"; exit;;
        * ) echo "
Please answer [yes] or [no].";;
    esac
done

if [ -d /sys/firmware/efi ]; then

parted --script /dev/$DRIVE \
mklabel gpt \
mkpart "efi" fat32 1MiB 257MiB \
set 1 esp on \
mkpart "root" ext4 257MiB 100%

mkfs.vfat -F32 /dev/$DRIVE"$SUB"1
echo y|mkfs.ext4 /dev/$DRIVE"$SUB"2

mkdir /mnt/efi
mount /dev/$DRIVE"$SUB"1 /mnt/efi
mount /dev/$DRIVE"$SUB"2 /mnt

else

parted --script /dev/$DRIVE \
mklabel msdos \
mkpart primary ext4 1MiB 100%
set 1 boot on

echo y|mkfs.ext4 /dev/$DRIVE"$SUB"1

mount /dev/$DRIVE"$SUB"1 /mnt

fi

sleep 1
timedatectl set-ntp true

pacstrap /mnt base base-devel linux-zen linux-firmware grub networkmanager reflector firewalld

genfstab -U /mnt >> /mnt/etc/fstab

cat << EOF | arch-chroot /mnt

ln -sf /usr/share/zoneinfo/Africa/Casablanca /etc/localtime

hwclock --systohc

sed -i '177 s/^#//' /etc/locale.gen
locale-gen

echo "LANG=en_US.UTF-8" > /etc/locale.conf

echo "$HOST" > /etc/hostname

systemctl enable NetworkManager reflector.timer firewalld

if [ -d /sys/firmware/efi ]; then
pacman -S efibootmgr
grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB
else
grub-install --target=i386-pc /dev/$DRIVE
fi

pacman -Syyu
useradd -mG wheel $UN
EOF

echo "### ADDING USER ###"
echo -e "$PASS\n$PASS" | passwd -R /mnt
echo -e "$UPASS\n$UPASS" | passwd $UN -R /mnt

echo "###########################"
echo "### INSTALLING PACKAGES ###"
echo "###########################"
declare -a pkgs=(
"xorg-server"
"xorg-xrandr"
"xorg-xinit"
"xorg-xrdb"
"doas"
"fish"
"alsa-utils"
"pulseaudio"
"pulseaudio-alsa"
"ttf-jetbrains-mono"
"ttf-font-awesome"
"nitrogen"
"starship"
"firefox"
"neovim"
"sxiv"
"feh"
"fzf"
"git"
"man"
"exa"
"bat"
)

for x in "${pkgs[@]}"; do
	pacstrap /mnt "$x"
done

echo "################################"
echo "### INSTALLING VIDEO DRIVERS ###"
echo "################################"

lspci | grep VGA | grep NVIDIA && pacstrap /mnt nvidia-dkms
lspci | grep VGA | grep Intel && pacstrap /mnt xf86-video-intel
lspci | grep VGA | grep AMD && pacstrap /mnt xf86-video-amdgpu
lspci | grep VGA | grep Red && pacstrap /mnt xf86-video-fbdev

echo "##################################"
echo "### ENABLING MICROCODE UPDATES ###"
echo "##################################"

lscpu | grep "Model name" | grep Intel && pacstrap /mnt intel-ucode
lscpu | grep "Model name" | grep AMD && pacstrap /mnt amd-ucode

cat << ESF | arch-chroot /mnt

echo "#############################"
echo "### CONFIGURING SUDO/DOAS ###"
echo "#############################"

sed -i '85 s/^# //' /etc/sudoers
echo "permit :wheel" > /etc/doas.conf

echo "########################"
echo "### ACTICATING SOUND ###"
echo "########################"

amixer sset Master unmute
amixer sset Speaker unmute
amixer sset Headphone unmute

echo "######################"
echo "### ACTIVATING AUR ###"
echo "######################"

cd /home/$UN
sudo -u $UN git clone https://aur.archlinux.org/yay.git
cd yay
sudo -u $UN makepkg -si --noconfirm
cd ..
rm -fr yay

echo "###############################"
echo "### INSTALLING AUR PACKAGES ###"
echo "###############################"

sudo -u $UN yay -S --noconfirm picom-tryone-git shell-color-scripts

echo "##################################"
echo "### INSTALLING SUCKLESS BUILDS ###"
echo "##################################"

sudo -u $UN git clone https://gitlab.com/ubrrr/graffiti.git
cd graffiti
sh install.sh
cd ..

echo "########################"
echo "### COPYING DOTFILES ###"
echo "########################"

sudo -u $UN git clone https://gitlab.com/ubrrr/labsystem.git

cp -r labsystem/.rootconfig /root/
cd /root/
rm -fr .config
mv .rootconfig .config

cd /home/$UN/labsystem
cp launcher /usr/local/bin/
cp reflector.conf /etc/xdg/reflector/
sudo -u $UN cp -r .config /home/$UN/
sudo -u $UN cp .xinitrc /home/$UN/
sudo -u $UN cp .Xresources /home/$UN/
sudo -u $UN cp -r wallpapers /home/$UN/
cd ..

if [ "$INFO" = fr ] || [ "$INFO" = 2 ]; then
echo "KEYMAP=fr-latin1" > /etc/vconsole.conf
sed -i '7 s/^#//' .xinitrc
fi

sed -i '82 s/^# //' /etc/sudoers
sed -i '85 s/^/# /' /etc/sudoers

echo "##############################"
echo "### CHANGING TO FISH SHELL ###"
echo "##############################"

chsh -s /usr/bin/fish
chsh -s /usr/bin/fish $UN

echo "##########################"
echo "### GRUB CONFIGURATION ###"
echo "##########################"

sed -i '6 s/^/# /' /etc/default/grub
sed -i '7iGRUB_CMDLINE_LINUX_DEFAULT=""' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg

exit
ESF

umount -R /mnt

echo "
Now you can reboot!"
