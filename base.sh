#!/bin/sh

timedatectl set-ntp true &> /dev/null

while true ;do
read -p "
Choose keymap:
1) en
2) fr
Select [en] or [fr]: " INFO
case $INFO in
    "en"* ) break;;
    "fr"* ) break;;
    "1"* ) break;;
    "2"* ) break;;
    * ) echo "
Invalid option: $INFO";;
    esac
done

read -p "
Create hostname: " HOST

read -p "Set root password: " PASS

while true ;do
echo
lsblk
read -p "
Select drive to format, example [sda]: " DRIVE
case $DRIVE in
    sd* ) break;;
    vd* ) break;;
    nvme* ) export SUB=p && break;;
    * ) echo "
Invalid option: $DRIVE";;
    esac
done

if [ "$(ls /sys/firmware/efi/efivars/ | wc -l)" -gt 0 ]; then

parted --script /dev/$DRIVE \
mklabel gpt \
mkpart "efi" fat32 1MiB 261MiB \
set 1 esp on \
mkpart "root" ext4 261MiB 100%

mkfs.vfat -F32 /dev/$DRIVE"$SUB"1
mkfs.ext4 /dev/$DRIVE"$SUB"2

mkdir /mnt/efi
mount /dev/$DRIVE"$SUB"1 /mnt/efi
mount /dev/$DRIVE"$SUB"2 /mnt

else

parted --script /dev/$DRIVE \
mklabel msdos \
mkpart primary ext4 1MiB 100%
set 1 boot on

mkfs.ext4 /dev/$DRIVE"$SUB"1

mount /dev/$DRIVE"$SUB"1 /mnt

fi

pacstrap /mnt base base-devel linux-zen linux-firmware grub networkmanager

genfstab -U /mnt >> /mnt/etc/fstab

cat << EOF | arch-chroot /mnt

ln -sf /usr/share/zoneinfo/Africa/Casablanca /etc/localtime

hwclock --systohc

sed -i '177 s/^#//' /etc/locale.gen
locale-gen

echo "LANG=en_US.UTF-8" > /etc/locale.conf

if [ "$INFO" = fr ] || [ "$INFO" = 2 ]; then
echo "KEYMAP=fr-latin1" > /etc/vconsole.conf
fi

echo "$HOST" > /etc/hostname

systemctl enable NetworkManager

if [ "$(ls /sys/firmware/efi/efivars/ | wc -l)" -gt 0 ]; then
pacman -S efibootmgr
grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB --removable --recheck
grub-mkconfig -o /boot/grub/grub.cfg
else
grub-install --target=i386-pc /dev/$DRIVE
grub-mkconfig -o /boot/grub/grub.cfg
fi
exit
EOF

echo -e "$PASS\n$PASS" | passwd --root /mnt
umount -R /mnt
