#!/bin/sh

if [ "$(id -u)" = 1000 ]; then
	echo Must be root to run this script
	exit 1
fi
error() { \
	clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}

echo "### ADDING USER ###"
read -p "Username: " UN
useradd -mG wheel $UN
passwd $UN

while true ;do
read -p "
Choose keymap:
1) en
2) fr
Select [en] or [fr]: " INFO
case $INFO in
    "en"* ) break;;
    "fr"* ) break;;
    "1"* ) break;;
    "2"* ) break;;
    * ) echo "
Invalid option: $INFO";;
    esac
done

pacman -Syyu

echo "###########################"
echo "### INSTALLING PACKAGES ###"
echo "###########################"
declare -a pkgs=(
"xorg-server"
"xorg-xrandr"
"xorg-xinit"
"xorg-xrdb"
"doas"
"fish"
"alsa-utils"
"ttf-jetbrains-mono"
"ttf-font-awesome"
"nitrogen"
#"firefox"
#"gimp"
"neovim"
"sxiv"
"feh"
"fzf"
"git"
"man"
)

for x in "${pkgs[@]}"; do
	pacman --noconfirm --needed -S "$x"
done

echo "################################"
echo "### INSTALLING VIDEO DRIVERS ###"
echo "################################"

if [ "$(lspci | grep VGA | grep NVIDIA | wc -l)" -gt 0 ]; then
	echo Installing Nvidia drivers
	pacman -S --noconfirm nvidia
fi

if [ "$(lspci | grep VGA | grep Intel | wc -l)" -gt 0 ]; then
	echo Installing Intel drivers
	pacman -S --noconfirm xf86-video-intel
fi

if [ "$(lspci | grep VGA | grep AMD | wc -l)" -gt 0 ]; then
	echo Installing AMD drivers
	pacman -S --noconfirm xf86-video-amdgpu
fi

if [ "$(lspci | grep VGA | grep Red | wc -l)" -gt 0 ]; then
	echo Installing video drivers
	pacman -S --noconfirm xf86-video-fbdev
fi

echo "#############################"
echo "### CONFIGURING SUDO/DOAS ###"
echo "#############################"

sed -i '85 s/^# //' /etc/sudoers
echo "permit :wheel" > /etc/doas.conf

echo "########################"
echo "### ACTICATING SOUND ###"
echo "########################"

amixer sset Master unmute
amixer sset Speaker unmute
amixer sset Headphone unmute

echo "######################"
echo "### ACTIVATING AUR ###"
echo "######################"

cd /home/$UN
sudo -u $UN git clone https://aur.archlinux.org/yay.git
cd yay
sudo -u $UN makepkg -si --noconfirm
cd ..
rm -fr yay

echo "###############################"
echo "### INSTALLING AUR PACKAGES ###"
echo "###############################"

sudo -u $UN yay -S --noconfirm picom-tryone-git shell-color-scripts

echo "##################################"
echo "### INSTALLING SUCKLESS BUILDS ###"
echo "##################################"

git clone https://gitlab.com/ubrrr/graffiti.git
cd graffiti
sh install.sh
cd ..

echo "########################"
echo "### COPYING DOTFILES ###"
echo "########################"

sudo -u $UN git clone https://gitlab.com/ubrrr/labsystem.git
cd labsystem
sudo -u $UN cp -r nvim/ /home/$UN/.config/
sudo -u $UN cp -r picom/ /home/$UN/.config/
sudo -u $UN mkdir /home/$UN/.config/fish
sudo -u $UN cp config.fish /home/$UN/.config/fish/
cp -r nvim/ /root/.config/
mkdir /root/.config/fish
cp config.root.fish /root/.config/fish/config.fish
cp launcher /usr/local/bin/
sudo -u $UN cp .xinitrc /home/$UN/
sudo -u $UN cp .Xresources /home/$UN/
sudo -u $UN cp -r wallpapers/ /home/$UN/
cd ..

if [ "$INFO" = fr ] || [ "$INFO" = 2 ]; then
sed -i '7 s/^#//' .xinitrc
fi

sed -i '82 s/^# //' /etc/sudoers
sed -i '85 s/^/# /' /etc/sudoers

echo "##############################"
echo "### CHANGING TO FISH SHELL ###"
echo "##############################"

chsh -s /usr/bin/fish
chsh -s /usr/bin/fish $UN

echo Now you can reboot!
